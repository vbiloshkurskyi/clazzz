package com.test.queue;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * Created by biloshkurskyi on 1/22/17.
 */
public class MostRecentlyInsertedQueue<E> extends AbstractQueue<E> implements Queue<E> {

    private int capacity;
    private int total;

    private Node head, tail;

    private class Node {
        private E item;
        private Node next;
    }

    public MostRecentlyInsertedQueue(int capacity) {
        if(capacity <= 0) {
            throw new IllegalArgumentException("capacity should be > 0");
        }

        this.total = 0;
        this.capacity = capacity;
        this.tail = null;
        this.head = null;
    }

    public Iterator<E> iterator() {
        return new ListIterator();
    }

    public int size() {
        return total;
    }

    public boolean offer(E e) {
        if(getCapacity() == 0) {
            poll();
        }

        Node oldTail = tail;
        tail = new Node();
        tail.item = e;
        tail.next = null;

        if(isEmpty()) {
            head = tail;
        } else {
            oldTail.next = tail;
        }
        total++;
        return true;
    }

    public E poll() {
        if (isEmpty()) {
            return null;
        }

        E item = head.item;
        head = head.next;

        total--;
        if (isEmpty()) {
            tail = null;
        }
        return item;
    }

    public E peek() {
        if(head != null) {
            return head.item;
        }
        return null;
    }

    private int getCapacity() {
        return capacity - total;
    }

    private class ListIterator implements Iterator<E> {
        private Node current = head;

        public boolean hasNext()  { return current != null;                     }
        public void remove()      { throw new UnsupportedOperationException();  }

        public E next() {
            if (!hasNext()) throw new NoSuchElementException();
            E item = current.item;
            current = current.next;
            return item;
        }
    }
}
