package com.test.queue;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;


public class MostRecentlyInsertedBlockingQueue <E> extends AbstractQueue<E> implements Queue<E> {

    private int capacity;
    private int total;

    private Node head, tail;

    private class Node {
        private E item;
        private Node next;
    }

    private final ReentrantLock lock;

    private final Condition notEmpty;

    private final Condition notFull;

    public MostRecentlyInsertedBlockingQueue(int capacity, boolean fair) {
        if(capacity <= 0) {
            throw new IllegalArgumentException("capacity should be > 0");
        }

        this.total = 0;
        this.capacity = capacity;
        this.tail = null;
        this.head = null;

        lock = new ReentrantLock(fair);
        notEmpty = lock.newCondition();
        notFull =  lock.newCondition();
    }

    public Iterator<E> iterator() {
        return new ListIterator();
    }

    public int size() {
        return total;
    }

    public boolean offer(E e) {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            if(getCapacity() == 0) {
                poll();
            }
            putToQueue(e);
            return true;
        } finally {
            lock.unlock();
        }
    }

    private void putToQueue(E e) {
        Node oldTail = tail;
        tail = new Node();
        tail.item = e;
        tail.next = null;

        if(isEmpty()) {
            head = tail;
        } else {
            oldTail.next = tail;
        }
        total++;
        notEmpty.signal();
    }

    private E getFromQueue() {
        if (isEmpty()) {
            return null;
        }

        E item = head.item;
        head = head.next;

        total--;
        if (isEmpty()) {
            tail = null;
        }
        notFull.signal();
        return item;
    }

    public E poll() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            if(isEmpty()) {
                return null;
            }
            return getFromQueue();
        } finally {
            lock.unlock();
        }
    }

    public E peek() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            if(head != null) {
                return head.item;
            }
            return null;
        } finally {
            lock.unlock();
        }
    }

    private int getCapacity() {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return total - capacity;
        } finally {
            lock.unlock();
        }
    }

    private class ListIterator implements Iterator<E> {
        private Node current = head;

        public boolean hasNext()  { return current != null;                     }
        public void remove()      { throw new UnsupportedOperationException();  }

        public E next() {
            if (!hasNext()) throw new NoSuchElementException();
            E item = current.item;
            current = current.next;
            return item;
        }
    }
}
