package com.test.queue;

import org.junit.Before;
import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.*;


public class ConcurrentMostRecentlyInsertedQueueTest {
    private Queue<Integer> queue;

    @Before
    public void setUp() throws Exception {
        queue = new ConcurrentMostRecentlyInsertedQueue<Integer>(3);
    }

    @Test
    public void testCRUD() throws Exception {
        queue.offer(1); // queue.size(): 1, contents (head -> tail): [ 1 ]
        assertEquals(new Integer(1), queue.iterator().next());

        queue.offer(2); // queue.size(): 2, contents (head -> tail): [ 1, 2 ]
        assertEquals("Expected size 2", 2, queue.size());
        assertEquals(new Integer(1), queue.toArray()[0]);
        assertEquals(new Integer(2), queue.toArray()[1]);

        queue.offer(3); // queue.size(): 3, contents (head -> tail): [ 1, 2, 3 ]
        assertEquals("Expected size 3", 3, queue.size());
        assertEquals(new Integer(1), queue.toArray()[0]);
        assertEquals(new Integer(2), queue.toArray()[1]);
        assertEquals(new Integer(3), queue.toArray()[2]);

        queue.offer(4); // queue.size(): 3, contents (head -> tail): [ 2, 3, 4 ]
        assertEquals("Expected size 3", 3, queue.size());
        assertEquals(new Integer(2), queue.toArray()[0]);
        assertEquals(new Integer(3), queue.toArray()[1]);
        assertEquals(new Integer(4), queue.toArray()[2]);


        queue.offer(5); // queue.size(): 3, contents (head -> tail): [ 3, 4, 5 ]
        assertEquals("Expected size 3", 3, queue.size());
        assertEquals(new Integer(3), queue.toArray()[0]);
        assertEquals(new Integer(4), queue.toArray()[1]);
        assertEquals(new Integer(5), queue.toArray()[2]);


        Integer poll1 = queue.poll(); // queue.size(): 2, contents (head -> tail): [ 4, 5 ], poll1 = 3
        assertEquals("Expected size 2", 2, queue.size());
        assertEquals(new Integer(3), poll1);
        assertEquals(new Integer(4), queue.toArray()[0]);
        assertEquals(new Integer(5), queue.toArray()[1]);

        Integer poll2 = queue.poll(); // queue.size(): 1, contents (head -> tail): [ 5 ], poll2 = 4
        assertEquals("Expected size 1", 1, queue.size());
        assertEquals(new Integer(4), poll2);
        assertEquals(new Integer(5), queue.toArray()[0]);

        queue.clear();
        assertTrue(queue.size() == 0);
    }
}